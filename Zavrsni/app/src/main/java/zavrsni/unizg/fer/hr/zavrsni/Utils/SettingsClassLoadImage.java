package zavrsni.unizg.fer.hr.zavrsni.Utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import zavrsni.unizg.fer.hr.zavrsni.Data.AppData;
import zavrsni.unizg.fer.hr.zavrsni.Elements.Scheme;
import zavrsni.unizg.fer.hr.zavrsni.LoadImageActivity;
import zavrsni.unizg.fer.hr.zavrsni.NetlistRecreation;
import zavrsni.unizg.fer.hr.zavrsni.R;

import static org.opencv.core.Core.FONT_HERSHEY_SIMPLEX;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.RETR_TREE;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.drawingMat;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.lastBitmap;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.imageView;

import static zavrsni.unizg.fer.hr.zavrsni.LoadImageActivity.listOfTexts;
import static zavrsni.unizg.fer.hr.zavrsni.Utils.HelperCVFunctions.findContours;

/**
 * Created by matej on 16/03/2018.
 */

public class SettingsClassLoadImage {
    private MyTessOCR mTessOCR;
    private boolean tessInitialized=false;
    static boolean textFound=false;
    public ImageButton settingsCamera;
    public FrameLayout settingsCameraLayout;
    public ToggleButton switchOriginal, binaryGaussSwitch, morphOpsSwitch, blurSwitch;
    public LinearLayout binaryGaussSettings, morphOpsSettings, blurSettings;
    public List<ToggleButton> flags = new ArrayList<>();
    public int C=8;
    public int erodeSize =3, dilateSize=8, classicBlurSize=1, medianBlurSize=1;
    public SeekBar seekC, seekErodeSize, seekDilateSize, seekClassicBlurSize, seekMedianBlurSize;
    public Bitmap bitmap =null, original=null;
    public Mat mat1, hierarchy;
    public Button findContours, detectWire, netlist, findText;
    public Scheme scheme;
    private ConstraintLayout drawingFlags;
    private ElementDetectionClass detection;
    private List<MatOfPoint> contours, trueContours;

    public SettingsClassLoadImage(AppCompatActivity context, View imageVie){
        new Thread(() -> {
            mTessOCR = new MyTessOCR(context);
            tessInitialized=true;
        }).start();
        drawingFlags=context.findViewById(R.id.drawingFlags);
        flags.add(context.findViewById(R.id.drawVodic));
        flags.add(context.findViewById(R.id.drawPotencijali));
        flags.add(context.findViewById(R.id.drawOtpornici));
        flags.add(context.findViewById(R.id.drawSI));
        flags.add(context.findViewById(R.id.drawAmpermeters));
        flags.add(context.findViewById(R.id.drawVoltmeters));
        flags.add(context.findViewById(R.id.drawSources));
        flags.add(context.findViewById(R.id.drawText));
        for (ToggleButton t : flags){
            t.setOnClickListener(flagListener);
        }
        netlist=context.findViewById(R.id.generateNetlist);
        findText=context.findViewById(R.id.findText);
        findContours= context.findViewById(R.id.findContours);
        detectWire= context.findViewById(R.id.detectWire);
        imageView = (ImageView) imageVie;
        seekC=context.findViewById(R.id.seekC);
        seekErodeSize= context.findViewById(R.id.erodeSize);
        seekDilateSize= context.findViewById(R.id.dilateSize);
        morphOpsSwitch=context.findViewById(R.id.morphOpsSwitch);
        morphOpsSettings=context.findViewById(R.id.morphOpsSettings);
        binaryGaussSwitch=context.findViewById(R.id.binaryGaussSwitch);
        binaryGaussSettings=context.findViewById(R.id.binaryGaussSettings);
        blurSettings=context.findViewById(R.id.blurSettings);
        blurSwitch = context.findViewById(R.id.blurSwitch);
        seekClassicBlurSize = context.findViewById(R.id.seekClassicBlurSize);
        seekMedianBlurSize=context.findViewById(R.id.seekMedianBlurSize);
        switchOriginal=context.findViewById(R.id.switchOriginal);
        settingsCamera=context.findViewById(R.id.settingsCamera);
        settingsCameraLayout=context.findViewById(R.id.settingsCameraLayout);
        findText.setOnClickListener(v -> {
            new Thread(() -> {
                try {
                    detectText();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        });
        netlist.setOnClickListener(v -> {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String currentDateandTime = sdf.format(new Date());
            String fileName="netlist" + currentDateandTime.trim() +".txt";
            scheme.writeNetlistFileOnInternalStorage(context, fileName);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            File sdcard = Environment.getExternalStorageDirectory();
            File file = new File(sdcard.getAbsolutePath() + "/Netlists", fileName);
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
            }
            catch (IOException e) {
                Toast.makeText(context, "Could not open " + file.getPath(), Toast.LENGTH_SHORT).show();
            }

            builder.setTitle("Netlist");
            builder.setMessage("Show netlist txt or recreate scheme with netlist?");
            builder.setPositiveButton("Recreate",
                    (dialog, id) ->{
                        Intent intent=new Intent(context, NetlistRecreation.class);
                        intent.putExtra("netlist", text.toString());
                        context.startActivity(intent);
                    });
            builder.setNegativeButton("Show",
                    (dialog, id) -> {

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                        builder1.setMessage(text.toString())
                                .setCancelable(true)
                                .setPositiveButton("Okay", (dialog1, id1) -> dialog1.cancel()).show();
                    });

            builder.setNeutralButton("Cancel",
                    (dialog, id) -> dialog.cancel());
            builder.create().show();
        });
        detectWire.setOnClickListener(v -> {
            if(contours != null && hierarchy!=null){
                binaryGaussSettings.setVisibility(View.GONE);
                morphOpsSettings.setVisibility(View.GONE);
                blurSettings.setVisibility(View.GONE);
                drawingFlags.setVisibility(View.VISIBLE);
                detection= new ElementDetectionClass(contours,
                        hierarchy,mat1,trueContours);
                detection.doDetection();
                scheme=detection.scheme;
                scheme.context=context;
            }
        });
        findContours.setOnClickListener(v -> {
            if(bitmap!=null) {
                mat1 = Mat.zeros(bitmap.getHeight(), bitmap.getWidth(), CvType.CV_8UC4);
                Utils.bitmapToMat(bitmap, mat1);
                Imgproc.cvtColor(mat1, mat1, Imgproc.COLOR_BGR2GRAY);
                Object[] temp=findContours(mat1);
                Mat matC=(Mat)temp[0];
                contours=(List<MatOfPoint>) temp[1];
                hierarchy=(Mat) temp[2];
                trueContours=(List<MatOfPoint>) temp[3];
                Utils.matToBitmap(matC, lastBitmap);
                imageView.setImageBitmap(lastBitmap);
            }
        });
        imageView.setOnTouchListener((v, event) -> {
            if(settingsCamera.getVisibility() == View.VISIBLE && drawingMat !=null){
                if (event.getAction() == android.view.MotionEvent.ACTION_UP && scheme!=null) {
                    scheme.AssignValue(new Point(event.getX(), event.getY()));
                }
            }
            return true;
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(settingsCamera.getVisibility() == View.VISIBLE)){
                    settingsCameraLayout.setVisibility(View.GONE);
                    settingsCamera.setVisibility(View.VISIBLE);
                }
            }
        });
        switchOriginal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switchOriginal.isChecked()){
                    if(original != null)
                        imageView.setImageBitmap(original);
                }else if(bitmap !=null){
                    imageView.setImageBitmap(bitmap);
                }
            }
        });
        settingsCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsCameraLayout.setVisibility(View.VISIBLE);
                settingsCamera.setVisibility(View.GONE);
            }
        });
        seekC.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                C=progress*4+30;
                processMat();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekErodeSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                erodeSize=progress+1;
                processMat();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekDilateSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                dilateSize=progress+1;
                processMat();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekClassicBlurSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                classicBlurSize=progress*2+1;
                processMat();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekMedianBlurSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                medianBlurSize=progress*2+1;
                processMat();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        binaryGaussSwitch.setOnClickListener(listener);
        morphOpsSwitch.setOnClickListener(listener);
        blurSwitch.setOnClickListener(listener);
    }

    private void detectText() throws InterruptedException {
        Mat mRgba = Mat.zeros(original.getHeight(),
                original.getWidth(), CvType.CV_8UC4);
        Utils.bitmapToMat(original, mRgba);
        Mat mGrey = Mat.zeros(imageView.getHeight(), imageView.getWidth(), CvType.CV_8UC4);
        Imgproc.cvtColor(mRgba, mGrey, Imgproc.COLOR_BGR2GRAY);

        listOfTexts.clear();
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();

        Rect rectan3;

        Imgproc.findContours(mat1, contours, hierarchy,
                RETR_TREE, CHAIN_APPROX_SIMPLE);
        Bitmap bmp;
        for (int ind = 0; ind < contours.size(); ind++) {
            rectan3 = Imgproc.boundingRect(contours.get(ind));
            povecajRect(rectan3);
            Imgproc.rectangle(mat1, rectan3.br(), rectan3.tl(), new Scalar(255));
            Mat croppedPart = mGrey.submat(rectan3);
            bmp = Bitmap.createBitmap(croppedPart.width(), croppedPart.height(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(croppedPart, bmp);

            String text = doOCR(bmp);
            if(text!= null){
                if(!text.isEmpty()){
                    listOfTexts.add(new Text(text, new Point(rectan3.x, rectan3.y)));
                    Imgproc.putText(mat1, text, new Point(rectan3.x, rectan3.y),FONT_HERSHEY_SIMPLEX,
                            1.5, new Scalar(255, 255, 255), 3 );
                }
            }
        }
        textFound=true;
        Utils.matToBitmap(mat1, lastBitmap);
        AppData.imageView.setImageBitmap(lastBitmap);
    }

    private static void povecajRect(Rect rectan3) {
        int prosirenje=3;
        rectan3.x =(int)(rectan3.x - prosirenje<1?1:rectan3.x - prosirenje);
        rectan3.y=(int)(rectan3.y - prosirenje<1?1:rectan3.y - prosirenje);
        rectan3.width=(int)(rectan3.width + 2*prosirenje>lastBitmap.getWidth()?
                lastBitmap.getWidth()-1:rectan3.width + 2*prosirenje);
        rectan3.height=(int)(rectan3.height + 2*prosirenje>lastBitmap.getHeight()?
                lastBitmap.getHeight()-1:rectan3.height + 2*prosirenje);
    }

    public String doOCR(final Bitmap bitmap) throws InterruptedException {
        while (true)
            //googlaj za sinkronizaciju dretvi
            if (tessInitialized)
                return mTessOCR.getOCRResult(bitmap);
            else
                Thread.sleep(100);
    }

    private void processMat(){
        if(bitmap!=null) {
            mat1 = Mat.zeros(bitmap.getHeight(), bitmap.getWidth(), CvType.CV_8UC4);
            Utils.bitmapToMat(bitmap, mat1);
            Imgproc.cvtColor(mat1, mat1, Imgproc.COLOR_BGR2GRAY);
            if(blurSwitch.isChecked())
                HelperCVFunctions.doBlur(mat1, classicBlurSize, medianBlurSize);
            if (binaryGaussSwitch.isChecked())
                HelperCVFunctions.binaryGaussian(mat1, C);
            if (morphOpsSwitch.isChecked()) {
                HelperCVFunctions.morphOpsErode(mat1, erodeSize);
                HelperCVFunctions.morphOpsDilate(mat1, dilateSize);
            }
            Utils.matToBitmap(mat1, lastBitmap);
            imageView.setImageBitmap(lastBitmap);
        }
    }

    View.OnClickListener listener= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            processMat();
            binaryGaussSettings.setVisibility(View.GONE);
            morphOpsSettings.setVisibility(View.GONE);
            blurSettings.setVisibility(View.GONE);
            if (v.getId()==binaryGaussSwitch.getId()){
                binaryGaussSettings.setVisibility(View.VISIBLE);
                morphOpsSwitch.setChecked(false);
                blurSwitch.setChecked(false);
            }else if(v.getId() == morphOpsSwitch.getId()){
                morphOpsSettings.setVisibility(View.VISIBLE);
                binaryGaussSwitch.setChecked(false);
                blurSwitch.setChecked(false);
            }else if(v.getId() == blurSwitch.getId()){
                blurSettings.setVisibility(View.VISIBLE);
                binaryGaussSwitch.setChecked(false);
                morphOpsSwitch.setChecked(false);
            }
            if(!(morphOpsSwitch.isChecked() && binaryGaussSwitch.isChecked() && blurSwitch.isChecked())){
                imageView.setImageBitmap(bitmap);
            }
        }
    };

    View.OnClickListener flagListener= v -> {
        boolean[] flagss = new boolean[8];
        for (int i=0; i<8;i++){
            flagss[i]=flags.get(i).isChecked();
        }
        detection.drawElements(flagss);
    };
}
