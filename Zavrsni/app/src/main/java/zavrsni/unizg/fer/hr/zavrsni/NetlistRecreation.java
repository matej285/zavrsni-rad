package zavrsni.unizg.fer.hr.zavrsni;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import zavrsni.unizg.fer.hr.zavrsni.Elements.Element;
import zavrsni.unizg.fer.hr.zavrsni.Utils.Text;

import static org.opencv.core.Core.FONT_HERSHEY_SIMPLEX;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.drawingMat;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.imageView;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.lastBitmap;

public class NetlistRecreation extends AppCompatActivity {
    ArrayList<Element> resistors = new ArrayList<>();
    ArrayList<Element> currentSources = new ArrayList<>();
    ArrayList<Element> voltmeters = new ArrayList<>();
    ArrayList<Element> ampermeters = new ArrayList<>();
    ArrayList<Element> sources = new ArrayList<>();
    ImageView imageViewNetlist;
    Mat drawingMat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_netlist_recreation);
        imageViewNetlist = findViewById(R.id.imageViewNetlist);
        String netlist = getIntent().getStringExtra("netlist");
        pasreNetlist(netlist);
        imageViewNetlist.setDrawingCacheEnabled(true);
        new Handler(msg -> false).postDelayed(this::drawElements, 100);
    }

    private void drawElements() {
        Bitmap b=Bitmap.createBitmap(imageViewNetlist.getWidth(), imageViewNetlist.getHeight(), Bitmap.Config.RGB_565);
        drawingMat = Mat.zeros(b.getHeight(), b.getWidth(), CvType.CV_8UC4);
        Imgproc.cvtColor(drawingMat, drawingMat, Imgproc.COLOR_BGR2GRAY);

        //nacrtaj vodic
        Scalar wireColor = new Scalar(255, 0, 0);

        //potencijali
        Random rng = new Random(12345);

        //otpornici
        Scalar resistorColor = new Scalar(0, 255, 0);
        drawElement(resistors);

        //strujni izvori
        Scalar currentColor = new Scalar(255, 100, 0);
        drawElement(currentSources);

        //ampermetri
        Scalar amperColor = new Scalar(255, 0, 100);
        drawElement(ampermeters);

        //voltmetri
        Scalar voltColor = new Scalar(0, 100, 255);
        drawElement(voltmeters);

        //izvor(i)
        drawElement(sources);
        Utils.matToBitmap(drawingMat, b);
        imageViewNetlist.setImageBitmap(b);
    }

    private void drawElement(ArrayList<Element> elementss){
        Scalar bijela=new Scalar(255,255,255);
        for (Element s : elementss){
            //tekst
            if (s.value != null && !s.value.text.equals("")) {
                Imgproc.line(drawingMat, s.nodeA, s.value.point, new Scalar(255));
                Imgproc.putText(drawingMat, s.value.text, s.value.point, FONT_HERSHEY_SIMPLEX,
                        1.5, new Scalar(255, 255, 255), 3);
            }
            Imgproc.circle(drawingMat, s.nodeA, 15, new Scalar(0,0,255), -1);
            if(sources.contains(s)){
                Imgproc.putText(drawingMat,String.valueOf(s.closestWireVrh) + "(+)",
                        new Point(s.nodeA.x-20, s.nodeA.y-20), FONT_HERSHEY_SIMPLEX, 1.5,
                        bijela, 3);
            }else
                Imgproc.putText(drawingMat,String.valueOf(s.closestWireVrh),
                        new Point(s.nodeA.x-20, s.nodeA.y-20), FONT_HERSHEY_SIMPLEX, 1.5,
                        bijela, 3);
            Imgproc.circle(drawingMat, s.nodeB, 15, new Scalar(0,0,255), -1);
            Imgproc.putText(drawingMat,String.valueOf(s.closestWireDno),
                    new Point(s.nodeB.x-20, s.nodeB.y-20), FONT_HERSHEY_SIMPLEX, 1.5,
                    bijela, 3);
        }
    }

    private void pasreNetlist(String netlist) {
        String[] lines = netlist.split("\n");
        for (String line : lines) {
            if (line.length() > 2) {
                String[] parts = line.split(" ");
                Element element = new Element(parts[0],
                        new Point(Double.parseDouble(parts[2].replaceAll("[{}]", "").split(",")[0]),
                                Double.parseDouble(parts[2].replaceAll("[{}]", "").split(",")[1])),
                        new Point(Double.parseDouble(parts[4].replaceAll("[{}]", "").split(",")[0]),
                                Double.parseDouble(parts[4].replaceAll("[{}]", "").split(",")[1])),
                        Integer.parseInt(parts[1]), Integer.parseInt(parts[3]));
                if(parts.length>5)
                    element.value=new Text(parts[5], element.nodeA);
                switch (parts[0].substring(0, 1)) {
                    case "R":
                        resistors.add(element);
                        break;
                    case "V":
                        voltmeters.add(element);
                        break;
                    case "A":
                        ampermeters.add(element);
                        break;
                    case "I":
                        sources.add(element);
                        break;
                    case "S":
                        currentSources.add(element);
                        break;
                }
            }
        }
    }
}
