package zavrsni.unizg.fer.hr.zavrsni;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.features2d.AgastFeatureDetector;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import zavrsni.unizg.fer.hr.zavrsni.Data.AppData;
import zavrsni.unizg.fer.hr.zavrsni.Utils.MyTessOCR;
import zavrsni.unizg.fer.hr.zavrsni.Utils.SettingsClassLoadImage;
import zavrsni.unizg.fer.hr.zavrsni.Utils.Text;
import zavrsni.unizg.fer.hr.zavrsni.Utils.TouchImageView;

import static org.opencv.core.Core.FONT_HERSHEY_SIMPLEX;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.RETR_TREE;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.imageView;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.lastBitmap;


public class LoadImageActivity extends AppCompatActivity {
    public TouchImageView imageView;
    public static ArrayList<Text> listOfTexts=new ArrayList<>();

    SettingsClassLoadImage settingsClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_image);
        Button saveImage = findViewById(R.id.saveImageButton);
        saveImage.setOnClickListener(v -> {
            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            FileOutputStream outStream = null;
            File sdCard = Environment.getExternalStorageDirectory();
            File dir = new File(sdCard.getAbsolutePath() + "/zavrsni_slike");
            dir.mkdirs();
            String fileName = String.format("%d.jpg", System.currentTimeMillis());
            File outFile = new File(dir, fileName);
            try {
                outStream = new FileOutputStream(outFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();
                Toast.makeText(getApplicationContext(), "image saved", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(dir));
            sendBroadcast(intent);
        });
        Button saveChange = findViewById(R.id.saveChange);
        saveChange.setOnClickListener(v -> {
            if(lastBitmap!=null){
                settingsClass.bitmap=lastBitmap.copy(lastBitmap.getConfig(), false);
            }
        });
        Button cutImage = findViewById(R.id.cropImage);
        cutImage.setOnClickListener(v -> {
            imageView.setDrawingCacheEnabled(true);
            Bitmap b=Bitmap.createBitmap(imageView.getDrawingCache());
            settingsClass.original=b;
            imageView.setImageBitmap(b);
            settingsClass.bitmap=b.copy(settingsClass.original.getConfig(), false);
            lastBitmap=b.copy(settingsClass.original.getConfig(), false);
            imageView.resetZoom();
        });
        Button resetImage = findViewById(R.id.resetImage);
        resetImage.setOnClickListener(v -> {
            AppData.setNull();
            recreate();
        });
        Button selectImageBtn = findViewById(R.id.loadImageButton);
        imageView=findViewById(R.id.imageViewLoad);
        settingsClass= new SettingsClassLoadImage(this,imageView);
        selectImageBtn.setOnClickListener(view -> {
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, 0);
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            settingsClass.original=getResizedBitmap(BitmapFactory.decodeFile(picturePath), imageView.getWidth(), imageView.getHeight());
            imageView.setImageBitmap(settingsClass.original);
            imageView.setMaxZoom(imageView.getMaxZoom()*3);
            settingsClass.bitmap=settingsClass.original.copy(settingsClass.original.getConfig(), false);
            lastBitmap=settingsClass.original.copy(settingsClass.original.getConfig(), false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!OpenCVLoader.initDebug())
            Toast.makeText(getApplicationContext(), "problem", Toast.LENGTH_SHORT).show();
        else
            baseLoaderCallback.onManagerConnected(BaseLoaderCallback.SUCCESS);
    }
    BaseLoaderCallback baseLoaderCallback= new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            super.onManagerConnected(status);
        }
    };

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

}
