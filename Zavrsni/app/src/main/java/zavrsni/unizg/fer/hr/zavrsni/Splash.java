package zavrsni.unizg.fer.hr.zavrsni;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Map;

public class Splash extends Activity {

    private static final int PERMISSION_ALL = 0;
    private Handler h;
    private Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        h = new Handler();

        r = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash.this, LoadImageActivity.class));
                finish();
            }
        };

        String[] PERMISSIONS = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        };

        if(!UtilPermissions.hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            setContentView(R.layout.activity_splash);
            h.postDelayed(r, 1500);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        int index = 0;
        Map<String, Integer> PermissionsMap = new HashMap<>();
        for (String permission : permissions){
            PermissionsMap.put(permission, grantResults[index]);
            index++;
        }

        if((PermissionsMap.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) != 0)
                || (PermissionsMap.get(Manifest.permission.READ_EXTERNAL_STORAGE) != 0)
                || (PermissionsMap.get(Manifest.permission.CAMERA) != 0)){
            Toast.makeText(this, "Permissions are obligatory", Toast.LENGTH_SHORT).show();
            finish();
        }
        else
            h.postDelayed(r, 1500);
    }
}

