package zavrsni.unizg.fer.hr.zavrsni.Data;

import android.graphics.Bitmap;
import android.widget.ImageView;

import org.opencv.core.Mat;

public class AppData {
    public static Mat drawingMat;
    public static Bitmap lastBitmap;
    public static ImageView imageView;

    public static void setNull(){
        drawingMat =null;
        lastBitmap=null;
        imageView=null;
    }
}
