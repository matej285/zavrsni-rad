package zavrsni.unizg.fer.hr.zavrsni.Utils;

import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import zavrsni.unizg.fer.hr.zavrsni.Data.AppData;
import zavrsni.unizg.fer.hr.zavrsni.Elements.Ampermeter;
import zavrsni.unizg.fer.hr.zavrsni.Elements.Element;
import zavrsni.unizg.fer.hr.zavrsni.Elements.Resistor;
import zavrsni.unizg.fer.hr.zavrsni.Elements.Scheme;
import zavrsni.unizg.fer.hr.zavrsni.Elements.Source;
import zavrsni.unizg.fer.hr.zavrsni.Elements.Voltmeter;
import zavrsni.unizg.fer.hr.zavrsni.LoadImageActivity;

import static org.opencv.core.Core.FONT_HERSHEY_SIMPLEX;
import static org.opencv.core.Core.min;
import static org.opencv.core.Core.sumElems;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.drawingMat;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.imageView;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.lastBitmap;
import static zavrsni.unizg.fer.hr.zavrsni.Utils.SettingsClassLoadImage.textFound;

public class ElementDetectionClass {
    HashMap<String, ArrayList<Point>> izv= new HashMap<>();
    List<MatOfPoint> source = new ArrayList<>();
    int wireNumber=-1;
    int maxWplusH=-1;
    List<MatOfPoint> contours, trueContours;
    Mat helperBinaryMat;
    Mat hierarchy;
    ArrayList<Integer> wires= new ArrayList<>();
    ArrayList<Integer> resistors= new ArrayList<>();
    ArrayList<Element> resistorss= new ArrayList<>();
    ArrayList<Integer> circles= new ArrayList<>();
    ArrayList<Integer> currentSources= new ArrayList<>();
    ArrayList<Element> currentSourcess= new ArrayList<>();
    ArrayList<Integer> voltmeters=new ArrayList<>();
    ArrayList<Element> voltmeterss= new ArrayList<>();
    ArrayList<Integer> ampermeters=new ArrayList<>();
    ArrayList<Element> ampermeterss= new ArrayList<>();
    ArrayList<Element> sourcess= new ArrayList<>();
    List<MatOfPoint2f> wiresByPotential= new ArrayList<>();
    List<MatOfPoint2f> wiresForAmpermeter= new ArrayList<>();
    Scheme scheme = new Scheme();
    double scale=Double.MAX_VALUE;
    private final int MINPLUSAREA = 5*5;
    private final int MAXPLUSAREA = 120*120;
    private final int MAXDISTANCETOPLUS= 75;
    private final int MAXDISTANCETOARROW= 180;


    public ElementDetectionClass(List<MatOfPoint> contours, Mat hierarchy,
                                 Mat helperBinaryMat,List<MatOfPoint> trueContours) {
        this.contours=contours;
        this.hierarchy=hierarchy;
        this.helperBinaryMat=helperBinaryMat;
        this.trueContours=trueContours;
        scheme.resistorss=resistorss;
        scheme.sourcess=sourcess;
        scheme.currentSourcess=currentSourcess;
        scheme.ampermeterss=ampermeterss;
        scheme.voltmeterss=voltmeterss;
    }

    public void doDetection(){
        detectMainWire();

        detectResistorsCirclesOtherWires();

        detectCircles();

        findDistanceScale();

        wiresByPotential();

        detectSource();

        attachNodesToElements();

        attatchTextToElements();

        drawElements(new boolean[]{true,true,true,true,true,true,true,true});
    }

    private void attatchTextToElements() {
        if (textFound) {
            for(Text t : LoadImageActivity.listOfTexts) {
                String s=t.text.replaceAll("\\s", "");
                s=s.replaceAll("[a-zA-Z]", "");
                if(!isNumeric(s)) continue;
                Point sredina=new Point();
                double minDistance=Double.MAX_VALUE;
                Element minElement = null;
                for (Element e : scheme.AllElements()) {
                    sredina= new Point((e.nodeA.x+e.nodeB.x)/2,(e.nodeA.y+e.nodeB.y)/2);
                    if(distance(sredina,t.point)<minDistance){
                        minDistance=distance(sredina, t.point);
                        minElement=e;
                    }
                }
                if(minElement.value==null)
                    minElement.value=t;
                else if(distance(minElement.value.point, sredina)>distance(t.point, sredina))
                    minElement.value=t;
            }
        }

    }

    private boolean isNumeric(String str)
    {
        try
        {
            Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public void attachNodesToElements() {
        for(int i: resistors){
            Rect rect = Imgproc.boundingRect(contours.get(i));
            Point vrh;
            Point dno;
            if(rect.height > rect.width){
                vrh = new Point(rect.x + rect.width/2, rect.y);
                dno = new Point(rect.x + rect.width/2, rect.y + rect.height);
            }else{
                vrh = new Point(rect.x, rect.y + rect.height/2);
                dno = new Point(rect.x + rect.width, rect.y  + rect.height/2);

            }
            Object[] a=findNodePointAndWire(vrh);
            Object[] b=findNodePointAndWire(dno);
            Integer closestWireVrh=(int)a[0];
            Integer closestWireDno=(int)b[0];
            Point nodeA=(Point) a[1];
            Point nodeB= (Point) b[1];

            resistorss.add(new Resistor("R"+closestWireVrh.toString() + closestWireDno.toString(),
                        nodeA,nodeB,closestWireVrh,closestWireDno));
        }
        for (int i : voltmeters){
            Object[] o=attachNodesCircularShape(contours.get(i));
            voltmeterss.add(new Voltmeter("V"+((Integer)o[0]).toString() + ((Integer)o[1]).toString(),
                    (Point) o[2],(Point) o[3], (Integer)o[0], (Integer)o[1]));
        }
        for (int i : ampermeters){
            Object[] o=attachNodesCircularShape(contours.get(i));
            ampermeterss.add(new Ampermeter("A"+((Integer)o[0]).toString() + ((Integer)o[1]).toString(),
                    (Point) o[2],(Point) o[3], (Integer)o[0], (Integer)o[1]));
        }
        for (int i : currentSources){
            Object[] o=attachNodesCircularShape(contours.get(i));

            int plus=determinePlusSideForCurrentSource((Point) o[2], (Point) o[3]);
            if(plus != 0) {
                if(plus==1){
                    currentSourcess.add(new Source("I"+((Integer)o[0]).toString() + ((Integer)o[1]).toString(),
                            (Point) o[2],(Point) o[3], (Integer)o[0], (Integer)o[1]));
                }else {
                    currentSourcess.add(new Source("I"+((Integer)o[1]).toString() + ((Integer)o[0]).toString(),
                            (Point) o[3],(Point) o[2], (Integer)o[1], (Integer)o[0]));
                }
            }else{
                currentSourcess.add(new Source("I"+((Integer)o[0]).toString() + ((Integer)o[1]).toString(),
                        (Point) o[2],(Point) o[3], (Integer)o[0], (Integer)o[1]));
            }
        }
        for (MatOfPoint contour : source){
            Rect rect = Imgproc.boundingRect(contour);
            Point vrh;
            Point dno;
            if(rect.width>rect.height){
                vrh = new Point(rect.x + rect.width/2, rect.y);
                dno = new Point(rect.x + rect.width/2, rect.y + rect.height);
            }else{
                vrh = new Point(rect.x, rect.y + rect.height/2);
                dno = new Point(rect.x + rect.width, rect.y  + rect.height/2);
            }
            Object[] a=findNodePointAndWire(vrh);
            Object[] b=findNodePointAndWire(dno);
            Integer closestWireVrh = (int) a[0];
            Integer closestWireDno = (int) b[0];

            int plus=determinePlusSide((Point) a[1], (Point) b[1]);
            if(plus != 0) {
                if(plus==1){
                    sourcess.add(new Source("S" + closestWireVrh.toString() + closestWireDno.toString(),
                            (Point) a[1], (Point) b[1], closestWireVrh, closestWireDno));
                }else {
                    sourcess.add(new Source("S" + closestWireDno.toString() + closestWireVrh.toString(),
                            (Point) b[1], (Point) a[1], closestWireDno, closestWireVrh));
                }
            }else {
                sourcess.add(new Source("S" + closestWireVrh.toString() + closestWireDno.toString(),
                        (Point) a[1], (Point) b[1], closestWireVrh, closestWireDno));
            }
        }

    }

    private int determinePlusSideForCurrentSource(Point nodeA, Point nodeB) {
        double longest=0;
        double lengthOfSource= distance(nodeA, nodeB);
        Point closest=null;
        for(int i=0 ; i<contours.size();i++){
            if(!(wires.contains(i) || circles.contains(i) || resistors.contains(i))) {
                MatOfPoint c = contours.get(i);
                Point pA = distanceToContour(nodeA, c.toArray());
                Point pB = distanceToContour(nodeB, c.toArray());
                double distanceToA=distance(pA, nodeA);
                double distanceToB=distance(pB, nodeB);
                if (distanceToA < MAXDISTANCETOARROW
                        && distanceToB < MAXDISTANCETOARROW) {
                    Rect rect =Imgproc.boundingRect(c);
                    double length=rect.width;
                    if(rect.height > rect.width) length=rect.height;
                    if (length < 1.7*lengthOfSource && length > 0.4*lengthOfSource) {
                        if(length>longest) {
                            longest = length;
                            Point center=center(c.toList());
                            Point vrhStrelice=rect.tl();
                            if(distance(rect.br(),center)< distance(rect.tl(),center)){
                                vrhStrelice=rect.br();
                            }
                            if(distance(nodeA,vrhStrelice)<distance(nodeB,vrhStrelice)){
                                closest=nodeA;
                            }else{
                                closest=nodeB;
                            }
                        }
                    }
                }
            }
        }
        if(closest!=null) {
            if (closest.equals(nodeA))
                return 1;
            return 2;
        }
        return 0;
    }

    private int determinePlusSide(Point nodeA, Point nodeB) {
        double minDistance=Double.MAX_VALUE;
        Point closest = null;
        for(int i=0 ; i<contours.size();i++){
            if(!(wires.contains(i) || circles.contains(i) || resistors.contains(i))) {
                MatOfPoint c = contours.get(i);
                Point p = distanceToContour(nodeA, c.toArray());
                double distanceToA=distance(p, nodeA);
                double distanceToB=distance(p, nodeB);
                if (distanceToA < MAXDISTANCETOPLUS
                        || distanceToB < MAXDISTANCETOPLUS) {
                    if (Imgproc.contourArea(c) < MAXPLUSAREA && Imgproc.contourArea(c) > MINPLUSAREA) {
                        if (distanceToA < distanceToB){
                            if(distanceToA<minDistance)
                                closest=nodeA;
                        }else{
                            if(distanceToB<minDistance)
                                closest=nodeB;
                        }
                    }
                }
            }
        }
        if(closest!=null) {
            if (closest.equals(nodeA))
                return 1;
            return 2;
        }
        return 0;
    }

    private Object[] attachNodesCircularShape(MatOfPoint contour){
        Rect rect = Imgproc.boundingRect(contour);
        Point vrh1 = new Point(rect.x + rect.width/2, rect.y);
        Point dno1 = new Point(rect.x + rect.width/2, rect.y + rect.height);
        Point vrh2 = new Point(rect.x, rect.y + rect.height/2);
        Point dno2 = new Point(rect.x + rect.width, rect.y  + rect.height/2);
        Object[] a1=findNodePointAndWire(vrh1);
        Object[] b1=findNodePointAndWire(dno1);
        Object[] a2=findNodePointAndWire(vrh2);
        Object[] b2=findNodePointAndWire(dno2);
        Object[] a;
        Object[] b;
        if(distance((Point)a1[1],vrh1)< distance((Point)a2[1], vrh2)) {
            a=a1;
            b=b1;
        }else{
            a=a2;
            b=b2;
        }
        Integer closestWireVrh = (int) a[0];
        Integer closestWireDno = (int) b[0];
        Point nodeA = (Point) a[1];
        Point nodeB = (Point) b[1];
        return new Object[]{closestWireVrh,closestWireDno,nodeA,nodeB};
    }

    public Object[] findNodePointAndWire(Point vrh){
        Integer closestWire=null;
        Point node=null;
        double minDistance=Double.MAX_VALUE;
        for(int j=0; j< wiresByPotential.size();j++){
            Point closestPoint = distanceToContour(vrh, wiresByPotential.get(j).toArray());
            if(distance(closestPoint, vrh)< minDistance){
                minDistance=distance(closestPoint,vrh);
                closestWire=j;
                Point srediste = new Point((vrh.x + vrh.x) / 2,
                        (closestPoint.y + closestPoint.y) / 2);
                node=srediste;
            }
        }
        return new Object[]{closestWire, node};
    }

    public void wiresByPotential() {
        for(int i=0; i<trueContours.size();i++){
                if (resistors.contains(i) || currentSources.contains(i) || voltmeters.contains(i) || ampermeters.contains(i)) {
                    Imgproc.drawContours(helperBinaryMat, trueContours, i, new Scalar(0, 0, 0), -1);
                    Imgproc.drawContours(helperBinaryMat, trueContours, i, new Scalar(0, 0, 0), (int)(3*scale+20));
                }
                if (!wires.contains(i))
                    Imgproc.drawContours(helperBinaryMat, trueContours, i, new Scalar(0, 0, 0), -1);
        }
        Mat temp=new Mat();
        helperBinaryMat.copyTo(temp);
        wiresByPotential=findWires();
        temp.copyTo(helperBinaryMat);
        for(int j=0; j<trueContours.size();j++){
            if(ampermeters.contains(j)){
                Imgproc.drawContours(helperBinaryMat, trueContours, j, new Scalar(0, 0, 0), -1);
                Imgproc.drawContours(helperBinaryMat, trueContours, j, new Scalar(0, 0, 0), (int)(3*scale+20));
            }
        }
        wiresForAmpermeter=findWires();
    }

    private List<MatOfPoint2f> findWires(){
        List<MatOfPoint2f> newWires= new ArrayList<>();
        Object[] temp= HelperCVFunctions.findContours(helperBinaryMat);
        ((Mat)temp[0]).copyTo(helperBinaryMat);
        int i=0;
        while (((Mat)temp[2]).get(0,i)[0] != -1){
            MatOfPoint2f newMtx = new MatOfPoint2f(((List<MatOfPoint>)temp[1])
                    .get(i).toArray());
            i=(int)((Mat)temp[2]).get(0,i)[0];
            newWires.add(newMtx);
        };
        MatOfPoint2f newMtx = new MatOfPoint2f(((List<MatOfPoint>)temp[1])
                .get(i).toArray());
        i=(int)((Mat)temp[2]).get(0,i)[0];
        Imgproc.approxPolyDP(newMtx, newMtx, 10, true);
        newWires.add(newMtx);
        return newWires;
    }

    public void drawElements(boolean[] flags) {
        drawingMat = Mat.zeros(lastBitmap.getHeight(), lastBitmap.getWidth(), CvType.CV_8UC4);
        if(flags[0]) {
            //nacrtaj vodic
            Scalar wireColor = new Scalar(255, 0, 0);
            for (int i : wires)
                Imgproc.drawContours(drawingMat, contours, i, wireColor, 4);
        }

        if(flags[1]) {
            //potencijali
            Random rng = new Random(12345);
            for (int i = 0; i < wiresByPotential.size(); i++) {
                Scalar color = new Scalar(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255));
                List<MatOfPoint> matOfPoint = new ArrayList<>();
                matOfPoint.add(new MatOfPoint(wiresByPotential.get(i).toArray()));
                Imgproc.drawContours(drawingMat, matOfPoint, 0, color, -1);
                Imgproc.drawContours(drawingMat, matOfPoint, 0, color, 2);
            }
        }

        if(flags[2]) {
            //otpornici
            Scalar resistorColor = new Scalar(0, 255, 0);
            for (int i : resistors)
                Imgproc.drawContours(drawingMat, contours, i, resistorColor, 3);
            drawElement(resistorss);
        }

        if(flags[3]) {
            //strujni izvori
            Scalar currentColor = new Scalar(255, 100, 0);
            for (int i : currentSources) {
                Imgproc.drawContours(drawingMat, contours, i, currentColor, 3);
                Imgproc.putText(drawingMat, "I", center(contours.get(i).toList()), FONT_HERSHEY_SIMPLEX,
                        1.5, currentColor, 3);
            }
            drawElement(currentSourcess);
        }

        if(flags[4]) {
            //ampermetri
            Scalar amperColor = new Scalar(255, 0, 100);
            for (int i : ampermeters) {
                Imgproc.drawContours(drawingMat, contours, i, amperColor, 3);
                Imgproc.putText(drawingMat, "A", center(contours.get(i).toList()), FONT_HERSHEY_SIMPLEX,
                        1.5, amperColor, 3);
            }
            drawElement(ampermeterss);
        }

        if(flags[5]) {
            //voltmetri
            Scalar voltColor = new Scalar(0, 100, 255);
            for (int i : voltmeters) {
                Imgproc.drawContours(drawingMat, contours, i, voltColor, 3);
                Imgproc.putText(drawingMat, "V", center(contours.get(i).toList()), FONT_HERSHEY_SIMPLEX,
                        1.5, voltColor, 3);
            }
            drawElement(voltmeterss);
        }

        if(flags[6]) {
            //izvor(i)
            for (int i = 0; i < source.size(); i++) {
                Imgproc.drawContours(drawingMat, source, i, new Scalar(0, 255, 255), 4);
            }
            drawElement(sourcess);
        }

        if(flags[7]) {
            //tekst
            for (Element e : scheme.AllElements()) {
                if (e.value != null && !e.value.text.equals("")) {
                    Imgproc.line(drawingMat, e.nodeA, e.value.point, new Scalar(255));
                    Imgproc.putText(drawingMat, e.value.text, e.value.point, FONT_HERSHEY_SIMPLEX,
                            1.5, new Scalar(255, 255, 255), 3);
                }
            }
        }
        Utils.matToBitmap(drawingMat, lastBitmap);
        imageView.setImageBitmap(lastBitmap);
    }


    private void drawElement(ArrayList<Element> elementss){
        Scalar bijela=new Scalar(255,255,255);
        for (Element s : elementss){
            Imgproc.circle(drawingMat, s.nodeA, 15, new Scalar(0,0,255), -1);
            if(sourcess.contains(s) || currentSourcess.contains(s)){
                Imgproc.putText(drawingMat,String.valueOf(s.closestWireVrh) + "(+)",
                        new Point(s.nodeA.x-20, s.nodeA.y-20), FONT_HERSHEY_SIMPLEX, 1.5,
                        bijela, 3);
            }else
            Imgproc.putText(drawingMat,String.valueOf(s.closestWireVrh),
                    new Point(s.nodeA.x-20, s.nodeA.y-20), FONT_HERSHEY_SIMPLEX, 1.5,
                    bijela, 3);
            Imgproc.circle(drawingMat, s.nodeB, 15, new Scalar(0,0,255), -1);
            Imgproc.putText(drawingMat,String.valueOf(s.closestWireDno),
                    new Point(s.nodeB.x-20, s.nodeB.y-20), FONT_HERSHEY_SIMPLEX, 1.5,
                    bijela, 3);
        }
    }

    public void detectCircles() {
        for(int c : circles){
            if(hierarchy.get(0,c)[2]!=-1){
                MatOfPoint insideCircle = contours.get((int)hierarchy.get(0,c)[2]);
                MatOfPoint2f triangle= new MatOfPoint2f();
                Imgproc.minEnclosingTriangle(insideCircle, triangle);
                double y0=triangle.toArray()[0].y;
                double y1=triangle.toArray()[1].y;
                double y2=triangle.toArray()[2].y;
                double distance01=Math.abs(y0-y1);
                double distance02=Math.abs(y0-y2);
                double distance12=Math.abs(y1-y2);
                if(distance01<distance02 && distance01<distance12){
                    if(y0<y2) voltmeters.add(c);
                    else ampermeters.add(c);
                }else if(distance02<distance01 && distance02<distance12){
                    if(y0<y1) voltmeters.add(c);
                    else ampermeters.add(c);
                }else{
                    if(y1<y0) voltmeters.add(c);
                    else ampermeters.add(c);
                }
            }else{
                currentSources.add(c);
            }
        }
    }

    public void detectResistorsCirclesOtherWires() {
        int childWire=(int)hierarchy.get(0,wireNumber)[2];
        while(childWire!=-1){
            MatOfPoint2f newMtx = new MatOfPoint2f( contours.get(childWire).toArray() );
            Imgproc.approxPolyDP(newMtx, newMtx, 15, true);
            if(Imgproc.contourArea(newMtx)<50*50) {
                //kontura premala, ignoriramo ju
            } //prepoznajemo otpornik
            else if(newMtx.total() < 7) {
                    resistors.add(childWire);
            }else{
                Point center=new Point();
                float[] radius=new float[1];
                Imgproc.minEnclosingCircle(new MatOfPoint2f(
                        contours.get(childWire).toArray()),center,  radius);
                Point[] points = newMtx.toArray();
                int i=0,k=0;
                for(Point p : points){
                    i++;
                    if(!(distance(p, center)>(0.85*radius[0]))){
                        //dozvolimo jednu pogresku
                        if(k == 0) {
                            k++;
                            continue;
                        }
                        //ako dvije tocke odstupaju od radiusa onda se radi o dijelu vodica
                        else if (Imgproc.contourArea(newMtx) > 110 * 110){
                            wires.add(childWire);
                        }
                        break;
                    }
                    //prosli smo sve tocke, dodajemo krug
                    if(points.length== i) {
                        circles.add(childWire);
                    }
                }
            }
            //sljedeci
            childWire=(int)hierarchy.get(0,childWire)[0];
        }
    }


    public void detectMainWire(){
        // wireNumber je broj elementa s najvecim W+H
        for (int i=0; i<contours.size(); i++){
            MatOfPoint matOfPoint=contours.get(i);
            Rect rect = Imgproc.boundingRect(matOfPoint);
            if((rect.height + rect.width) >= maxWplusH){
                maxWplusH=rect.height+rect.width;
                wireNumber=i;
            }
        }
        wires.add(wireNumber);
    }

    public void detectSource(){
        for(int i=0; i<wiresByPotential.size();i++) {
            MatOfPoint2f wire2f = wiresByPotential.get(i);
            Point[] wirePnts1 = wire2f.toArray();
            for (int j=i+1; j<wiresByPotential.size();j++) {
                izv.put(String.valueOf(i).concat(String.valueOf(j)), new ArrayList<Point>());
                MatOfPoint2f w2f=wiresByPotential.get(j);
                Point[] wirePnts2 = w2f.toArray();
                for (Point wirePoint1 : wirePnts1) {
                    for(Point wirePoint2 : wirePnts2) {
                        if (distance(wirePoint1, wirePoint2) < (int)(2*scale+30)) {
                            Point srediste = new Point((wirePoint1.x + wirePoint2.x) / 2,
                                    (wirePoint1.y + wirePoint2.y) / 2);
                            izv.get(String.valueOf(i).concat(String.valueOf(j))).add(srediste);
                        }
                    }
                }
            }
        }

        //ako smo nasli izvor
        int k=0;
        for(int i=0; i<wiresByPotential.size();i++) {
            for (int j = i + 1; j < wiresByPotential.size(); j++) {
                if (!izv.get(String.valueOf(i).concat(String.valueOf(j))).isEmpty()) {
                    source.add(new MatOfPoint());
                    source.get(k).fromList(izv.get(String.valueOf(i).concat(String.valueOf(j))));
                    MatOfInt hull = new MatOfInt();
                    Imgproc.convexHull(source.get(k), hull);
                    source.set(k, convertIndexesToPoints(source.get(k), hull));
                    k++;
                }
            }
        }
    }

    public void findDistanceScale() {
        if(resistors.isEmpty()){
            scale=20;
        }else
        for(int w:wires) {
            Point minDistPoint = distanceToContour(contours.get(resistors.get(0)).toArray()[0],
                    contours.get(w).toArray());
            if(distance(contours.get(resistors.get(0)).toArray()[0], minDistPoint)<scale){
                scale=distance(contours.get(resistors.get(0)).toArray()[0], minDistPoint);
            }
        }
        Log.d("scale", String.valueOf(scale));
        if(scale<15) scale=15;
        if(scale>45) scale=45;
    }



    public static double distance(Point a, Point b){
        return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
    }

    private Point distanceToContour(Point a, Point[] points){
        if(points.length <= 0) return new Point(0,0);
        double minDistance=distance(a, points[0]);
        Point minDistancePoint=points[0];
        for(Point point : points){
            if(distance(point, a)< minDistance){
                minDistance=distance(point, a);
                minDistancePoint=point;
            }
        }
        return minDistancePoint;
    }

    public static Point center(List<Point> points){
        double sumX=0;
        double sumY=0;
        for(Point point : points){
            sumX += point.x;
            sumY += point.y;
        }
        return new Point(sumX/points.size(), sumY/points.size());
    }

    public static MatOfPoint convertIndexesToPoints(MatOfPoint contour, MatOfInt indexes) {
        int[] arrIndex = indexes.toArray();
        Point[] arrContour = contour.toArray();
        Point[] arrPoints = new Point[arrIndex.length];

        for (int i=0;i<arrIndex.length;i++) {
            arrPoints[i] = arrContour[arrIndex[i]];
        }

        MatOfPoint hull = new MatOfPoint();
        hull.fromArray(arrPoints);
        return hull;
    }
}
