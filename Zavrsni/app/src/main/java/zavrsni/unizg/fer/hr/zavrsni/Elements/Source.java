package zavrsni.unizg.fer.hr.zavrsni.Elements;

import org.opencv.core.Point;

public class Source extends Element{

    public Source(String name, Point nodeA, Point nodeB, int closestWireVrh, int closestWireDno) {
        super(name, nodeA, nodeB, closestWireVrh, closestWireDno);
    }
}
