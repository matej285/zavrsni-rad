package zavrsni.unizg.fer.hr.zavrsni.Elements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.text.InputType;
import android.widget.EditText;
import android.widget.Toast;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import zavrsni.unizg.fer.hr.zavrsni.Utils.ElementDetectionClass;
import zavrsni.unizg.fer.hr.zavrsni.Utils.Text;

import static org.opencv.core.Core.FONT_HERSHEY_SIMPLEX;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.drawingMat;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.imageView;
import static zavrsni.unizg.fer.hr.zavrsni.Data.AppData.lastBitmap;

public class Scheme {
    public ArrayList<Element> resistorss;
    public ArrayList<Element> currentSourcess;
    public ArrayList<Element> voltmeterss;
    public ArrayList<Element> ampermeterss;
    public ArrayList<Element> sourcess;
    public Context context;

    public ArrayList<Element> AllElements() {
        ArrayList<Element> allElements = new ArrayList<>();
        allElements.addAll(ampermeterss);
        allElements.addAll(voltmeterss);
        allElements.addAll(currentSourcess);
        allElements.addAll(sourcess);
        allElements.addAll(resistorss);
        return allElements;
    }

    public void AssignValue(Point p) {
        for (Element e : AllElements()) {
            Point sredina = new Point((e.nodeA.x + e.nodeB.x) / 2, (e.nodeA.y + e.nodeB.y) / 2);
            double polumjer = ElementDetectionClass.distance(e.nodeA, e.nodeB) / 2;
            if (ElementDetectionClass.distance(p, sredina) < polumjer) {
                showDialog(e, sredina);
                break;
            }
        }
    }

    void showDialog(Element e, Point sredina) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Enter value");

        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                e.value=new Text(input.getText().toString(), sredina);
                if(e.value!=null) {
                    Imgproc.putText(drawingMat, e.value.text, e.value.point, FONT_HERSHEY_SIMPLEX,
                            1.5, new Scalar(255, 255, 255), 3);
                    Utils.matToBitmap(drawingMat, lastBitmap);
                    imageView.setImageBitmap(lastBitmap);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void writeNetlistFileOnInternalStorage(Context mcoContext,String sFileName){
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File (sdCard.getAbsolutePath() + "/Netlists");
        if(!directory.exists()){
            directory.mkdir();
        }

        try{
            File gpxfile = new File(directory, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            for (Element e: AllElements()) {
                writer.append(e.toString() + "\n");
            }
            writer.flush();
            writer.close();
            Toast.makeText(mcoContext, directory.getPath(), Toast.LENGTH_SHORT).show();

        }catch (IOException e){
            e.printStackTrace();

        }
    }
}
