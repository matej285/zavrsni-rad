package zavrsni.unizg.fer.hr.zavrsni.Elements;

import org.opencv.core.Point;

import zavrsni.unizg.fer.hr.zavrsni.Utils.Text;

public class Element {
    public String name;
    public Point nodeA;
    public Point nodeB;
    public int closestWireVrh;
    public int closestWireDno;
    public Text value = new Text("", new Point(0,0));

    public Element(String name, Point nodeA, Point nodeB, int closestWireVrh, int closestWireDno) {
        this.name = name;
        this.nodeA = nodeA;
        this.nodeB = nodeB;
        this.closestWireVrh = closestWireVrh;
        this.closestWireDno = closestWireDno;
    }

    @Override
    public String toString() {
        return name + " " + closestWireVrh + " " + nodeA + " " + closestWireDno + " " + nodeB + " " + value.text;
    }
}
