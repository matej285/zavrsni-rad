package zavrsni.unizg.fer.hr.zavrsni.Utils;

import org.opencv.core.Point;

public class Text {
    public String text;
    public Point point;

    public Text(String text, Point point) {
        this.text = text;
        this.point = point;
    }
}
