package zavrsni.unizg.fer.hr.zavrsni.Elements;

import org.opencv.core.Point;

public class Resistor extends Element{

    public Resistor(String name, Point nodeA, Point nodeB, int closestWireVrh, int closestWireDno) {
        super(name, nodeA, nodeB, closestWireVrh, closestWireDno);
    }
}
