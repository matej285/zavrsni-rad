package zavrsni.unizg.fer.hr.zavrsni.Utils;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import zavrsni.unizg.fer.hr.zavrsni.LoadImageActivity;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.core.CvType.CV_8UC4;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.RETR_TREE;

/**
 * Created by matej on 15/03/2018.
 */

public class HelperCVFunctions {

    public static void binaryGaussian(Mat mat1, int C){
        Imgproc.threshold(mat1, mat1, C, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C);
    }

    public static void morphOpsErode(Mat thresholdedImage, int size1) {
        Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(size1, size1));
        Imgproc.erode(thresholdedImage, thresholdedImage, erodeElement);
        Imgproc.erode(thresholdedImage, thresholdedImage, erodeElement);
    }

    public static void morphOpsDilate(Mat thresholdedImage, int size2) {
        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(size2, size2));
        Imgproc.dilate(thresholdedImage, thresholdedImage, dilateElement);
        Imgproc.dilate(thresholdedImage, thresholdedImage, dilateElement);
    }

    public static void doBlur(Mat mat1, int classic, int median){
        Imgproc.blur(mat1, mat1, new Size( classic, classic ), new Point(-1,-1));
        Imgproc.medianBlur(mat1, mat1, median);
    }

    public static Object[] findContours(Mat mat1){
        Random rng=new Random(12345);
        List<MatOfPoint> contours = new ArrayList<>();
        List<MatOfPoint> approxContours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(mat1, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
        Mat drawing=Mat.zeros(mat1.size(), CV_8UC3);
        for (int i = 0; i< contours.size(); i++)
        {
            MatOfPoint2f newMtx = new MatOfPoint2f( contours.get(i).toArray() );
            Imgproc.approxPolyDP(newMtx, newMtx, 3.1, true);
            approxContours.add(new MatOfPoint(newMtx.toArray()));
            Point[] points=newMtx.toArray();
            for(Point p: points){
                Imgproc.circle(drawing, p, 8, new Scalar(0,255,0), 5);
                Imgproc.circle(drawing, p, 12, new Scalar(255,0,0), 3);
            }
        }
        for (int i = 0; i< approxContours.size(); i++){
            Scalar color = new Scalar(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255));
            Imgproc.drawContours(drawing, approxContours, i, color, 2, 8, hierarchy, 1,new  Point());

        }
            return new Object[]{drawing, approxContours, hierarchy, contours};
    }

}
